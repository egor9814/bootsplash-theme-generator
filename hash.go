package main

import (
	"crypto/sha256"
	"fmt"
	"strings"
)

func hashOf(data []byte) string {
	s := make([]string, 32)
	for i, it := range sha256.Sum256(data) {
		s[i] = fmt.Sprintf("%02x", it)
	}
	return strings.Join(s, "")
}
