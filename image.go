package main

import (
	"bytes"
	"os"
	"strconv"
	"strings"
)

type image struct {
	path     string
	position position
	offset   int
	index    int
}

func (i *image) Assemble(background string) (string, string) {
	buf := bytes.Buffer{}
	name := "IMAGE_" + strconv.Itoa(i.index)
	buf.WriteString(name + "=" + i.path + "\n")
	buf.WriteString(name + "_WIDTH=$(identify $" + name + " | head -1 | cut -d' ' -f3 | cut -d'x' -f1)\n")
	buf.WriteString(name + "_HEIGHT=$(identify $" + name + " | head -1 | cut -d' ' -f3 | cut -d'x' -f2)\n")
	buf.WriteString(name + "_COUNT=$(identify $" + name + " | wc -l)\n")
	buf.WriteString("convert -alpha remove -background \"" + background + "\" $" + name +
		" \"" + name + "_%0${#" + name + "_COUNT}d.rgb\"\n")
	buf.WriteString(name + "_BLOBS=\"\"\n")
	buf.WriteString("for it in $(ls " + name + "_*.rgb); do\n")
	buf.WriteString("\t" + name + "_BLOBS=\"$" + name + "_BLOBS --blob $it\"\n")
	buf.WriteString("done\n")
	s1 := buf.String()

	buf = bytes.Buffer{}
	buf.WriteString(" --picture")
	buf.WriteString(" --pic_width $" + name + "_WIDTH")
	buf.WriteString(" --pic_height $" + name + "_HEIGHT")
	buf.WriteString(" --pic_position " + i.position.String())
	if i.offset != 0 {
		buf.WriteString(" --pic_position_offset 0x" + strconv.FormatInt(int64(i.offset), 16))
	}
	buf.WriteString(" --pic_anim_type 1")
	buf.WriteString(" --pic_anim_loop 0")
	buf.WriteString(" $" + name + "_BLOBS")

	return s1, buf.String()
}

func (i *image) hash() (string, error) {
	data, err := os.ReadFile(i.path)
	if err != nil {
		return "", err
	}
	return hashOf(data), nil
}

func (i *image) Parse(ctx *context, index int, config map[string]interface{}) {
	i.index = index

	if v, ok := config["name"]; ok {
		if vs, ok := v.(string); ok {
			i.path = vs
		}
	}
	if len(i.path) == 0 {
		ctx.ErrorMessage("name not specified for image " + strconv.Itoa(index))
	}

	pos := ""
	if v, ok := config["position"]; ok {
		if vs, ok := v.(string); ok {
			pos = vs
		}
	}
	if len(pos) == 0 {
		i.position = 0
	} else if strings.ContainsRune(pos, '|') {
		i.position = parsePosition(strings.Split(pos, "|"))
	} else {
		i.position = parsePosition([]string{pos})
	}

	if v, ok := config["offset"]; ok {
		if vi, ok := v.(int); ok {
			i.offset = vi
		}
	}

	ctx.pkgbuild.sources = append(ctx.pkgbuild.sources, i.path)
	if ctx.pkgbuild.calculateHashSums && len(i.path) > 0 {
		h, err := i.hash()
		ctx.Error(err)
		ctx.pkgbuild.sha256sums = append(ctx.pkgbuild.sha256sums, h)
	} else {
		ctx.pkgbuild.sha256sums = append(ctx.pkgbuild.sha256sums, "SKIP")
	}
}
