package main

import (
	"bytes"
	"strconv"
	"strings"
)

type pkgbuild struct {
	themeName            string
	authors              []string
	name, ver, rel, desc string
	epoch                *string
	arch, license        []string
	url                  string
	sources, sha256sums  []string

	calculateHashSums bool
}

func (pkg *pkgbuild) Assemble(out *bytes.Buffer) {
	if l := len(pkg.authors); l == 1 {
		out.WriteString("# Maintainer: " + pkg.authors[0] + "\n\n")
	} else if l > 1 {
		for i, it := range pkg.authors {
			if i == 0 {
				out.WriteString("# Maintainers: " + it + "\n")
			} else {
				out.WriteString("#              " + it + "\n")
			}
		}
		out.WriteString("\n")
	}

	out.WriteString("pkgname=('" + pkg.name + "')\n")
	out.WriteString("pkgver=" + pkg.ver + "\n")
	out.WriteString("pkgrel=" + pkg.rel + "\n")
	if pkg.epoch != nil {
		out.WriteString("epoch=" + *pkg.epoch + "\n")
	}
	out.WriteString("pkgdesc=\"" + pkg.desc + "\"\n")

	arr := make([]string, len(pkg.arch))
	for i, it := range pkg.arch {
		arr[i] = "'" + it + "'"
	}
	out.WriteString("arch=(" + strings.Join(arr, "\n\t") + ")\n")

	arr = make([]string, len(pkg.license))
	for i, it := range pkg.license {
		arr[i] = "'" + it + "'"
	}
	out.WriteString("license=(" + strings.Join(arr, "\n\t") + ")\n")

	out.WriteString("depends=()\n")
	out.WriteString("optdepends=('bootsplash-systemd: for bootsplash functionality')\n")
	out.WriteString("builddepends=('imagemagick')\n")
	out.WriteString("options=('!libtool' '!emptydirs')\n")

	arr = make([]string, len(pkg.sources))
	for i, it := range pkg.sources {
		arr[i] = "'" + it + "'"
	}
	out.WriteString("source=(" + strings.Join(arr, "\n\t") + ")\n\n")

	arr = make([]string, len(pkg.sha256sums))
	for i, it := range pkg.sha256sums {
		arr[i] = "'" + it + "'"
	}
	out.WriteString("sha256sums=(" + strings.Join(arr, "\n\t") + ")\n\n")

	out.WriteString("build() {\n")
	out.WriteString("  cd \"$srcdir\"\n")
	out.WriteString("  chmod +x pack.sh\n")
	out.WriteString("  ./pack.sh\n")
	out.WriteString("}\n\n")

	out.WriteString("package_" + pkg.name + "() {\n")
	out.WriteString("  pkgdesc=\"" + pkg.desc + "\"\n")
	out.WriteString("  cd \"$srcdir\"\n")
	out.WriteString("  install -Dm644 \"$srcdir/bootsplash\" \"$pkgdir/usr/lib/firmware/bootsplash-themes/" +
		pkg.themeName + "/bootsplash\"\n")
	out.WriteString("  install -Dm644 \"$srcdir/initcpio\" \"$pkgdir/usr/lib/initcpio/install/bootsplash-" +
		pkg.themeName + "\"\n")
	out.WriteString("}\n\n")
}

func (pkg *pkgbuild) SrcInfo(out *bytes.Buffer) {
	out.WriteString("pkgbase = " + pkg.name + "\n")
	out.WriteString("\tpkgver = " + pkg.ver + "\n")
	out.WriteString("\tpkgrel = " + pkg.rel + "\n")
	out.WriteString("\tpkgdesc = " + pkg.desc + "\n")
	for _, it := range pkg.arch {
		out.WriteString("\tarch = " + it + "\n")
	}
	for _, it := range pkg.license {
		out.WriteString("\tlicense = " + it + "\n")
	}
	out.WriteString("\toptdepends = bootsplash-systemd: for bootsplash functionality\n")
	out.WriteString("\toptions = !libtool\n")
	out.WriteString("\toptions = !emptydirs\n\n")

	for _, it := range pkg.sources {
		out.WriteString("\tsource = " + it + "\n")
	}
	for _, it := range pkg.sha256sums {
		out.WriteString("\tsha256sums = " + it + "\n")
	}
	out.WriteString("\n")

	out.WriteString("pkgname = " + pkg.name + "\n")
	out.WriteString("\tpkgdesc = " + pkg.desc + "\n")
}

func (pkg *pkgbuild) Parse(ctx *context, config map[string]interface{}) {
	if name, ok := config["name"]; ok {
		if name, ok := name.(string); ok {
			if len(name) > 0 {
				pkg.themeName = name
			}
		}
	}
	if len(pkg.themeName) == 0 {
		ctx.ErrorMessage("theme name not specified")
	}

	if authors, ok := config["authors"]; ok {
		if authors, ok := authors.([]interface{}); ok {
			pkg.authors = make([]string, len(authors))
			for i, it := range authors {
				if s, ok := it.(string); ok {
					pkg.authors[i] = s
				}
			}
		}
	}

	if p, ok := config["package"]; ok {
		if p, ok := p.(map[string]interface{}); ok {
			if v, ok := p["name"]; ok {
				if vs, ok := v.(string); ok {
					pkg.name = vs
				}
			}

			if v, ok := p["description"]; ok {
				if vs, ok := v.(string); ok {
					pkg.desc = vs
				}
			}

			if v, ok := p["version"]; ok {
				if vs, ok := v.(string); ok {
					pkg.ver = vs
				} else if vi, ok := v.(int); ok {
					pkg.ver = strconv.Itoa(vi)
				}
			}

			if v, ok := p["release"]; ok {
				if vs, ok := v.(string); ok {
					pkg.rel = vs
				} else if vi, ok := v.(int); ok {
					pkg.rel = strconv.Itoa(vi)
				}
			}

			if v, ok := p["epoch"]; ok {
				if v != nil {
					if vs, ok := v.(string); ok {
						pkg.epoch = new(string)
						*pkg.epoch = vs
					} else if vi, ok := v.(int); ok {
						pkg.epoch = new(string)
						*pkg.epoch = strconv.Itoa(vi)
					}
				}
			}

			if v, ok := p["release"]; ok {
				if vs, ok := v.(string); ok {
					pkg.desc = vs
				}
			}

			if v, ok := p["arch"]; ok {
				if v, ok := v.([]interface{}); ok {
					pkg.arch = make([]string, len(v))
					for i, it := range v {
						if s, ok := it.(string); ok {
							pkg.arch[i] = s
						}
					}
				}
			}

			if v, ok := p["url"]; ok {
				if v != nil {
					if vs, ok := v.(string); ok {
						pkg.url = vs
					} else if vi, ok := v.(int); ok {
						pkg.url = strconv.Itoa(vi)
					}
				}
			}

			if v, ok := p["licenses"]; ok {
				if v, ok := v.([]interface{}); ok {
					pkg.license = make([]string, len(v))
					for i, it := range v {
						if s, ok := it.(string); ok {
							pkg.license[i] = s
						}
					}
				}
			}

			if v, ok := p["sha256sum"]; ok {
				if vb, ok := v.(bool); ok {
					pkg.calculateHashSums = vb
				}
			}
		}
	}
	if len(pkg.name) == 0 {
		ctx.ErrorMessage("package name not specified")
	}
	if len(pkg.ver) == 0 {
		pkg.ver = "unknown"
	}
	if len(pkg.rel) == 0 {
		pkg.rel = "1"
	}
	if pkg.epoch != nil && len(*pkg.epoch) == 0 {
		pkg.epoch = nil
	}
}
