package main

import (
	"fmt"
	"os"
)

func run() error {
	ctx := newContext()
	ctx.Assemble()
	return ctx.GetError()
}

func main() {
	if len(os.Args) > 1 {
		if os.Args[1] == "create" {
			if err := os.WriteFile("config.json", []byte(defaultConfig), 0644); err != nil {
				fmt.Println(err)
				os.Exit(1)
			}
		}
	} else if err := run(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
