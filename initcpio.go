package main

import (
	"bytes"
	"crypto/sha256"
	"fmt"
	"strings"
)

type initcpio struct {
	data string
}

func (i *initcpio) Assemble(out *bytes.Buffer) {
	out.WriteString(i.data)
}

func (i *initcpio) hash() (string, error) {
	s := make([]string, 32)
	for i, it := range sha256.Sum256([]byte(i.data)) {
		s[i] = fmt.Sprintf("%02x", it)
	}
	return strings.Join(s, ""), nil
}

func (i *initcpio) Parse(ctx *context) {
	i.data = strings.ReplaceAll(`build() {
	add_binary /usr/lib/firmware/bootsplash-themes/${THEME_NAME}/bootsplash /usr/lib/firmware/bootsplash-themes/${THEME_NAME}/bootsplash
}

help() {
	echo "no help for bootsplash-${THEME_NAME} hook"
}

#EOF
`, "${THEME_NAME}", ctx.pkgbuild.themeName)

	ctx.pkgbuild.sources = append(ctx.pkgbuild.sources, "initcpio")
	if ctx.pkgbuild.calculateHashSums {
		h, err := i.hash()
		ctx.Error(err)
		ctx.pkgbuild.sha256sums = append(ctx.pkgbuild.sha256sums, h)
	} else {
		ctx.pkgbuild.sha256sums = append(ctx.pkgbuild.sha256sums, "SKIP")
	}
}
