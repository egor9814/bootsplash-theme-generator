package main

var defaultConfig = `{
  "name": "awesome",
  "authors": [
    "Awesome Name <awesome@mail.domain>"
  ],
  "package": {
    "name": "bootsplash-theme-awesome",
    "version": "1.0",
    "release": 1,
    "epoch": null,
    "description": "Awesome description",
    "arch": [
      "x86_64"
    ],
    "url": null,
    "licenses": [],
    "sha256sum": true
  },
  "background": "darkgrey",
  "frame_ms": 48,
  "images": [
    {
      "name": "<path to image>",
      "position": "top|left"
    }
  ]
}`
