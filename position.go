package main

import "strings"

type position uint

const (
	left position = 1 << iota
	top
	right
	bottom
)

var positionMapping = [...]string{
	0:              "0",
	left | top:     "0x00",
	top:            "0x01",
	right | top:    "0x02",
	right:          "0x03",
	bottom | right: "0x04",
	bottom:         "0x05",
	bottom | left:  "0x06",
	left:           "0x07",
}

func (p position) String() string {
	return positionMapping[p]
}

func parsePosition(p []string) position {
	var result position
	for _, it := range p {
		switch strings.ToLower(it) {
		case "left", "l":
			result |= left
		case "top", "t":
			result |= top
		case "right", "r":
			result |= right
		case "bottom", "b":
			result |= bottom
		case "center", "c":
			result = 0
		}
	}
	return result
}
