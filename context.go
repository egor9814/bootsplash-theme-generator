package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"os"
)

type context struct {
	errors   []error
	pkgbuild *pkgbuild
	pack     *pack
	packer   *packer
	initcpio *initcpio
}

func newContext() *context {
	return &context{
		errors: make([]error, 0, 2),
	}
}

func (ctx *context) Error(err error) {
	if err != nil {
		ctx.errors = append(ctx.errors, err)
	}
}

func (ctx *context) ErrorMessage(msg string) {
	if len(msg) > 0 {
		ctx.errors = append(ctx.errors, errors.New(msg))
	}
}

func (ctx *context) GetError() error {
	if l := len(ctx.errors); l == 0 {
		return nil
	} else if l == 1 {
		return ctx.errors[0]
	}
	buf := bytes.Buffer{}
	for i, it := range ctx.errors {
		if i > 0 {
			buf.WriteString("\n")
		}
		buf.WriteString(it.Error())
	}
	return errors.New(buf.String())
}

func (ctx *context) Assemble() {
	jsonData, err := os.ReadFile("config.json")
	if err != nil {
		ctx.Error(err)
		return
	}
	jsonConfig := make(map[string]interface{})
	err = json.Unmarshal(jsonData, &jsonConfig)

	ctx.pkgbuild = new(pkgbuild)
	ctx.pkgbuild.Parse(ctx, jsonConfig)

	ctx.pack = new(pack)
	ctx.pack.Parse(ctx, jsonConfig)

	ctx.packer = new(packer)

	ctx.initcpio = new(initcpio)
	ctx.initcpio.Parse(ctx)

	out := bytes.Buffer{}
	ctx.pack.Assemble(ctx, &out)
	ctx.Error(os.WriteFile("pack.sh", out.Bytes(), 0744))

	out = bytes.Buffer{}
	ctx.initcpio.Assemble(&out)
	ctx.Error(os.WriteFile("initcpio", out.Bytes(), 0644))

	out = bytes.Buffer{}
	ctx.packer.Unpack(ctx, &out)
	ctx.Error(os.WriteFile("bootsplash-packer", out.Bytes(), 0744))

	out = bytes.Buffer{}
	ctx.pkgbuild.Assemble(&out)
	ctx.Error(os.WriteFile("PKGBUILD", out.Bytes(), 0644))

	out = bytes.Buffer{}
	ctx.pkgbuild.SrcInfo(&out)
	ctx.Error(os.WriteFile(".SRCINFO", out.Bytes(), 0644))
}
