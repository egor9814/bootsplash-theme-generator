package main

import (
	"bytes"
)

type packer struct{}

func (p *packer) hash() string {
	return hashOf(packerData)
}

func (p *packer) Unpack(ctx *context, out *bytes.Buffer) {
	ctx.pkgbuild.sources = append(ctx.pkgbuild.sources, "bootsplash-packer")
	if ctx.pkgbuild.calculateHashSums {
		h := p.hash()
		ctx.pkgbuild.sha256sums = append(ctx.pkgbuild.sha256sums, h)
	} else {
		ctx.pkgbuild.sha256sums = append(ctx.pkgbuild.sha256sums, "SKIP")
	}
	out.Write(packerData)
}
